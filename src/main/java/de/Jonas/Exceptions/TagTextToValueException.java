package de.Jonas.Exceptions;

public class TagTextToValueException extends Exception {
    public TagTextToValueException(String s) {
        super(s);
    }

    public TagTextToValueException() {
        super();
    }
}
