package de.Jonas.GCBanner.GUI;

import de.Jonas.Exceptions.TagTextToValueException;
import de.Jonas.GCBanner.Logic.Banner;
import de.Jonas.GCBanner.Logic.Utility;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Date;

public class BannerView extends Stage {

    private Banner banner = null;
    private String group = null;
    private boolean linkChanged;
    private String languagePrefix;

    private TextField tf_rawlink = new TextField();
    private CheckBox cb_rawlink = new CheckBox(Language.getString(this.languagePrefix + "cbRawLink"));
    private Label lb_link = new Label();

    public BannerView(Stage s) {
        this.initOwner(s);
        this.initModality(Modality.WINDOW_MODAL);
        this.banner = null;
        this.group = null;
        this.languagePrefix = "bannerViewAdd_";
    }

    public BannerView(Stage s, Banner banner, String group) {
        this(s);
        this.banner = banner;
        this.group = group;
        this.languagePrefix = "bannerViewChange_";
    }

    public void showView(String[] groups, DataGroupCallback<Banner> cb) {
        this.linkChanged = false;

        GridPane gp = new GridPane();

        Label lb_rawlink = new Label(Language.getString(this.languagePrefix + "lbRawLink"));
        Label lb_type = new Label(Language.getString(this.languagePrefix + "lbType"));
        Label lb_gccode = new Label(Language.getString(this.languagePrefix + "lbGccode"));
        Label lb_width = new Label(Language.getString(this.languagePrefix + "lbWidth"));
        Label lb_height = new Label(Language.getString(this.languagePrefix + "lbHeight"));
        Label lb_comment = new Label(Language.getString(this.languagePrefix + "lbComment"));
        ChoiceBox<String> cb_type = new ChoiceBox<>();
        TextField tf_gccode = new TextField();
        SpinnerAutoCommit<Integer> s_width = new SpinnerAutoCommit<>(0, 1000, 0);
        SpinnerAutoCommit<Integer> s_height = new SpinnerAutoCommit<>(0, 1000, 150);
        TextField tf_comment = new TextField();
        Button btn_ok = new Button(Language.getString(this.languagePrefix + "btnOK"));
        Button btn_cancel = new Button(Language.getString(this.languagePrefix + "btnCancel"));

        gp.setPadding(new Insets(5));
        gp.setHgap(5);
        gp.setVgap(5);

        cb_type.getItems().setAll(groups);

        btn_ok.setOnAction(event -> {
            String rawLink = tf_rawlink.getText();
            String link;
            String type = cb_type.getValue();
            String gccode = tf_gccode.getText();
            int width = s_width.getValue();
            int height = s_height.getValue();
            String comment = tf_comment.getText();

            if (this.banner == null || this.linkChanged) {
                try {
                    if (this.cb_rawlink.isSelected()) {
                        link = this.tf_rawlink.getText();
                    } else {
                        link = Utility.tagTextToValue(rawLink, "img", "src");
                    }
                    if (this.banner != null) {
                        this.banner.setLink(link);
                    } else {
                        this.banner = new Banner(link);
                    }
                } catch (TagTextToValueException e) {
                    return;
                }
            }

            if (type == null) { return; }

            this.banner.setGccode(gccode);
            this.banner.setWidth(width);
            this.banner.setHeight(height);
            this.banner.setComment(comment);
            this.banner.setCreationDate(new Date());

            cb.callback(this.banner, type);

            this.close();
        });

        btn_cancel.setOnAction(event -> this.close());

        tf_rawlink.textProperty().addListener((observable, oldValue, newValue) -> {
            this.updateLinkText();
        });

        cb_rawlink.setOnAction(event -> {
            this.updateLinkText();
        });

        s_width.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000));
        s_width.setEditable(true);
        s_height.setEditable(true);

        if (this.banner != null) {
            //set content of input boxes
            lb_link.setText(this.banner.getLink());
            cb_type.getSelectionModel().select(cb_type.getItems().indexOf(this.group));
            tf_gccode.setText(this.banner.getGccode());
            s_width.getValueFactory().setValue(this.banner.getWidth());
            s_height.getValueFactory().setValue(this.banner.getHeight());
            tf_comment.setText(this.banner.getComment());
        }

        gp.add(lb_rawlink, 0,0);
        gp.add(lb_type, 0, 2);
        gp.add(lb_gccode, 0, 3);
        gp.add(lb_width, 0, 4);
        gp.add(lb_height, 0, 5);
        gp.add(lb_comment, 0, 6);
        gp.add(tf_rawlink, 1, 0, 2, 1);
        gp.add(cb_rawlink, 3, 0);
        gp.add(lb_link, 1, 1, 3, 1);
        gp.add(cb_type, 1, 2, 3, 1);
        gp.add(tf_gccode, 1, 3, 3, 1);
        gp.add(s_width, 1, 4, 3, 1);
        gp.add(s_height, 1, 5, 3, 1);
        gp.add(tf_comment, 1, 6, 3, 1);
        gp.add(btn_ok, 1, 7);
        gp.add(btn_cancel, 2, 7);

        Scene scene = new Scene(gp);

        this.setTitle(Language.getString(this.languagePrefix + "title"));
        this.setScene(scene);
        this.show();
    }

    private void updateLinkText() {
        this.linkChanged = true;
        String newValue = this.tf_rawlink.getText();
        if (this.cb_rawlink.isSelected()) {
            this.lb_link.setText(newValue);
            this.lb_link.setTextFill(Color.web("#000000"));
        } else {
            try {
                this.lb_link.setText(Utility.tagTextToValue(newValue, "img", "src"));
                this.lb_link.setTextFill(Color.web("#000000"));
            } catch (TagTextToValueException e) {
                this.lb_link.setText(Language.getString(this.languagePrefix + "linkNotFound"));
                this.lb_link.setTextFill(Color.web("#ff0000"));
            }
        }
    }
}
