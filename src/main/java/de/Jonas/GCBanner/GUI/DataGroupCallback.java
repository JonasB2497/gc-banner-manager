package de.Jonas.GCBanner.GUI;

public interface DataGroupCallback<T> {
    void callback(T data, String group);
}
