package de.Jonas.GCBanner.GUI;

import java.util.HashMap;
import java.util.Map;

public class Language {

    public static enum lang {
        EN,
        DE
    };

    private static Map<String, String> language = new HashMap<>();

    public static void load(lang l) {
        language.put("version", "v0.15");
        language.put("gitlab_url", "https://gitlab.com/JonasB2497/gc-banner-manager");
        switch (l) {
            case EN:
                loadEn();
                break;
            case DE:
                loadDe();
                break;
        }
    }

    private static void loadEn() {
        language.put("mainView_title", "GC-Banner Manager");
        language.put("mainView_btnAdd", "add");
        language.put("mainView_btnChange", "change");
        language.put("mainView_btnRemove", "remove");

        language.put("mainView_mFile", "File");
        language.put("mainView_mTypes", "Types");
        language.put("mainView_mAbout", "About");
        language.put("mainView_miLoad", "load");
        language.put("mainView_miSave", "save");
        language.put("mainView_miSaveAs", "save as");
        language.put("mainView_miExportHtml", "export as HTML");
        language.put("mainView_miExit", "exit");
        language.put("mainView_miShowTypes", "show");
        language.put("mainView_miAddType", "add");
        language.put("mainView_miVersion", "version:");

        language.put("mainView_fcLoad", "load file");
        language.put("mainView_fcSave", "save");
        language.put("mainView_fcSaveAs", "save as");
        language.put("mainView_fcExportHtml", "export HTML");

        language.put("mainView_fcExtensionFilter", "GC-Banner Manager files (*.gcbm)");
        language.put("mainView_fcExtensionFilterHtml", "HTML-files (*.html)");

        language.put("mainView_alertSaveOnExitTitle", "Save changes before closing?");
        language.put("mainView_alertSaveOnExit", "Your changes will be lost if you don't save them.");
        language.put("mainView_alertSaveOnExitSave", "Save");
        language.put("mainView_alertSaveOnExitDiscard", "Discard");
        language.put("mainView_alertSaveOnExitCancel", "Cancel");

        language.put("mainView_tfSearch", "search...");
        language.put("mainView_clearSearch", "x");

        language.put("mainView_tcLink", "link");
        language.put("mainView_tcType", "type");
        language.put("mainView_tcGccode", "GC-Code");
        language.put("mainView_tcWidth", "width");
        language.put("mainView_tcHeight", "height");
        language.put("mainView_tcComment", "comment");

        language.put("mainView_alertTypesTitle", "Banner types:");

        language.put("mainView_visitWebpage", "visit website");



        language.put("bannerViewAdd_title", "add Banner");
        language.put("bannerViewAdd_lbRawLink", "Link: ");
        language.put("bannerViewAdd_lbType", "Type: ");
        language.put("bannerViewAdd_lbGccode", "GC-Code: ");
        language.put("bannerViewAdd_lbWidth", "Width: ");
        language.put("bannerViewAdd_lbHeight", "Height: ");
        language.put("bannerViewAdd_lbComment", "Comment: ");
        language.put("bannerViewAdd_btnOK", "Add");
        language.put("bannerViewAdd_btnCancel", "Cancel");
        language.put("bannerViewAdd_linkNotFound", "no link found");

        language.put("bannerViewChange_title", "change Banner");
        language.put("bannerViewChange_lbRawLink", "Link: ");
        language.put("bannerViewChange_lbType", "Type: ");
        language.put("bannerViewChange_lbGccode", "GC-Code: ");
        language.put("bannerViewChange_lbWidth", "Width: ");
        language.put("bannerViewChange_lbHeight", "Height: ");
        language.put("bannerViewChange_lbComment", "Comment: ");
        language.put("bannerViewChange_btnOK", "Change");
        language.put("bannerViewChange_btnCancel", "Cancel");
        language.put("bannerViewChange_linkNotFound", "no link found");

        language.put("addTypeView_title", "add type");
        language.put("addTypeView_header", "please enter a new type name below");
        language.put("addTypeView_content", "Name:");

        language.put("defaultFileName_saveFile", "Banner");
        language.put("defaultFileName_exportHTML", "Banner");
    }

    public static void loadDe() {
        language.put("mainView_title", "GC-Banner Manager");
        language.put("mainView_btnAdd", "hinzufügen");
        language.put("mainView_btnChange", "ändern");
        language.put("mainView_btnRemove", "entfernen");

        language.put("mainView_mFile", "Datei");
        language.put("mainView_mTypes", "Gruppen");
        language.put("mainView_mAbout", "Über");
        language.put("mainView_miLoad", "laden");
        language.put("mainView_miSave", "speichern");
        language.put("mainView_miSaveAs", "speichern als");
        language.put("mainView_miExportHtml", "exportieren als HTML");
        language.put("mainView_miExit", "Beenden");
        language.put("mainView_miAddType", "hinzufügen");
        language.put("mainView_miShowTypes", "anzeigen");
        language.put("mainView_miVersion", "Version:");

        language.put("mainView_fcLoad", "Datei laden");
        language.put("mainView_fcSave", "speichern");
        language.put("mainView_fcSaveAs", "speichern als");
        language.put("mainView_fcExportHtml", "exportiere HTML");

        language.put("mainView_fcExtensionFilter", "GC-Banner Manager Dateien (*.gcbm)");
        language.put("mainView_fcExtensionFilterHtml", "HTML-Dateien (*.html)");

        language.put("mainView_alertSaveOnExitTitle", "Änderungen vor dem Beenden speichern?");
        language.put("mainView_alertSaveOnExit", "Änderungen werden für immer verloren sein, wenn sie nicht gespeichert werden");
        language.put("mainView_alertSaveOnExitSave", "Speichern");
        language.put("mainView_alertSaveOnExitDiscard", "Verwerfen");
        language.put("mainView_alertSaveOnExitCancel", "Abbrechen");

        language.put("mainView_tfSearch", "suchen...");
        language.put("mainView_clearSearch", "x");

        language.put("mainView_tcLink", "Link");
        language.put("mainView_tcType", "Gruppe");
        language.put("mainView_tcGccode", "GC-Code");
        language.put("mainView_tcWidth", "Breite");
        language.put("mainView_tcHeight", "Höhe");
        language.put("mainView_tcComment", "Kommentar");

        language.put("mainView_alertTypesTitle", "Banner Typen:");

        language.put("mainView_visitWebpage", "Website besuchen");


        language.put("bannerViewAdd_title", "Banner hinzufügen");
        language.put("bannerViewAdd_lbRawLink", "Link: ");
        language.put("bannerViewAdd_lbType", "Gruppe: ");
        language.put("bannerViewAdd_lbGccode", "GC-Code: ");
        language.put("bannerViewAdd_lbWidth", "Breite: ");
        language.put("bannerViewAdd_lbHeight", "Höhe: ");
        language.put("bannerViewAdd_lbComment", "Kommentar: ");
        language.put("bannerViewAdd_btnOK", "hinzufügen");
        language.put("bannerViewAdd_btnCancel", "abbrechen");
        language.put("bannerViewAdd_linkNotFound", "Link nicht gefunden");

        language.put("bannerViewChange_title", "Banner ändern");
        language.put("bannerViewChange_lbRawLink", "Link: ");
        language.put("bannerViewChange_lbType", "Gruppe: ");
        language.put("bannerViewChange_lbGccode", "GC-Code: ");
        language.put("bannerViewChange_lbWidth", "Breite: ");
        language.put("bannerViewChange_lbHeight", "Höhe: ");
        language.put("bannerViewChange_lbComment", "Kommentar: ");
        language.put("bannerViewChange_btnOK", "ändern");
        language.put("bannerViewChange_btnCancel", "abbrechen");
        language.put("bannerViewChange_linkNotFound", "Link nicht gefunden");

        language.put("addTypeView_title", "Gruppe hinzufügen");
        language.put("addTypeView_header", "Bitte gebe einen neuen Namen unten ein");
        language.put("addTypeView_content", "Name:");

        language.put("defaultFileName_saveFile", "Banner");
        language.put("defaultFileName_exportHTML", "Banner");
    }

    public static String getString(String key) {
        return language.get(key);
    }
}
