package de.Jonas.GCBanner.GUI;

import de.Jonas.GCBanner.Logic.Banner;
import de.Jonas.GCBanner.Logic.Database;
import de.Jonas.GCBanner.Storage.*;
import de.Jonas.GCBanner.Storage.Converter.BannerHTMLConverter;
import de.Jonas.GCBanner.Storage.Converter.BannerJSONConverter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public class MainView extends Application {

    private Database db = new Database();
    private ObservableList<Banner> ol_data = FXCollections.observableArrayList();
    private Stage stage;
    private String searchString = "";

    @Override
    public void start(Stage stage) {

        this.stage = stage;

        Language.load(Language.lang.DE);

        BorderPane bp = new BorderPane();
        VBox vb = new VBox();
        FlowPane fp = new FlowPane();

        MenuBar mb_main = new MenuBar();
        Menu m_file = new Menu(Language.getString("mainView_mFile"));
        Menu m_types = new Menu(Language.getString("mainView_mTypes"));
        Menu m_about = new Menu(Language.getString("mainView_mAbout"));
        MenuItem mi_load = new MenuItem(Language.getString("mainView_miLoad"));
        MenuItem mi_save = new MenuItem(Language.getString("mainView_miSave"));
        MenuItem mi_saveAs = new MenuItem(Language.getString("mainView_miSaveAs"));
        MenuItem mi_exportHtml = new MenuItem(Language.getString("mainView_miExportHtml"));
        MenuItem mi_exit = new MenuItem(Language.getString("mainView_miExit"));
        MenuItem mi_showTypes = new MenuItem(Language.getString("mainView_miShowTypes"));
        MenuItem mi_addType = new MenuItem(Language.getString("mainView_miAddType"));
        MenuItem mi_version = new MenuItem(Language.getString("mainView_miVersion") + " " + Language.getString("version"));
        MenuItem mi_gitlab = new MenuItem(Language.getString(("mainView_visitWebpage")));

        HBox hb_search = new HBox();
        TextField tf_search = new TextField();
        Button btn_clear = new Button(Language.getString("mainView_clearSearch"));

        TableView<Banner> tv_data = new TableView<>();
        TableColumn<Banner, String> tc_link = new TableColumn<>(Language.getString("mainView_tcLink"));
        TableColumn<Banner, String> tc_type = new TableColumn<>(Language.getString("mainView_tcType"));
        TableColumn<Banner, String> tc_gccode = new TableColumn<>(Language.getString("mainView_tcGccode"));
        TableColumn<Banner, Integer> tc_width = new TableColumn<>(Language.getString("mainView_tcWidth"));
        TableColumn<Banner, Integer> tc_height = new TableColumn<>(Language.getString("mainView_tcHeight"));
        TableColumn<Banner, String> tc_comment = new TableColumn<>(Language.getString("mainView_tcComment"));

        Button btn_add = new Button(Language.getString("mainView_btnAdd"));
        Button btn_change = new Button(Language.getString("mainView_btnChange"));
        Button btn_remove = new Button(Language.getString("mainView_btnRemove"));

        stage.setTitle(Language.getString("mainView_title"));

        //load menu operation
        mi_load.setOnAction(event -> {
            tv_data.setDisable(true);
            FileChooser fc = new FileChooser();
            fc.setTitle(Language.getString("mainView_fcLoad"));
            fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Language.getString("mainView_fcExtensionFilter"), "*.gcbm"));
            fc.setInitialFileName(Language.getString("defaultFileName_saveFile") + ".gcbm");
            File f = fc.showOpenDialog(stage);
            if (f != null) {
                final Task<Boolean> task = new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        loadFile(f);
                        return true;
                    }
                };
                task.setOnSucceeded( (v) -> {
                    tv_data.setDisable(false);
                });
                task.run();
            } else {
                tv_data.setDisable(false);
            }
        });
        //save menu operation
        mi_save.setOnAction(event -> {
            tv_data.setDisable(true);
            final Task<Boolean> task = new Task<Boolean>() {
                @Override
                protected Boolean call() throws Exception {
                    if (db.hasFormatter()) {
                        saveFile();
                    } else {
                        FileChooser fc = new FileChooser();
                        fc.setTitle(Language.getString("mainView_fcSave"));
                        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Language.getString("mainView_fcExtensionFilter"), "*.gcbm"));
                        fc.setInitialFileName(Language.getString("defaultFileName_saveFile") + ".gcbm");
                        File f = fc.showSaveDialog(stage);
                        if (f != null) {
                            saveFileAs(f);
                        }
                    }
                    return true;
                }
            };
            task.setOnSucceeded( (v) -> {
                tv_data.setDisable(false);
            });
            task.run();

        });
        //saveAs operation
        mi_saveAs.setOnAction(event -> {
            tv_data.setDisable(true);
            final Task<Boolean> task = new Task<Boolean>() {
                @Override
                protected Boolean call() throws Exception {
                    FileChooser fc = new FileChooser();
                    fc.setTitle(Language.getString("mainView_fcSaveAs"));
                    fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Language.getString("mainView_fcExtensionFilter"), "*.gcbm"));
                    fc.setInitialFileName(Language.getString("defaultFileName_saveFile") + ".gcbm");
                    File f = fc.showSaveDialog(stage);
                    if (f != null) {
                        saveFileAs(f);
                    }
                    return true;
                }
            };
            task.setOnSucceeded((v) -> {
                tv_data.setDisable(false);
            });
            task.run();
        });
        //export operation
        mi_exportHtml.setOnAction(event -> {
            tv_data.setDisable(true);
            final Task<Boolean> task = new Task<Boolean>() {
                @Override
                protected Boolean call() throws Exception {
                    FileChooser fc = new FileChooser();
                    fc.setTitle(Language.getString("mainView_fcExport"));
                    fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Language.getString("mainView_fcExtensionFilterHtml"), "*.html"));
                    fc.setInitialFileName(Language.getString("defaultFileName_exportHTML") + ".html");
                    File f = fc.showSaveDialog(stage);
                    if (f != null) {
                        exportHTML(f);
                    }
                    return true;
                }
            };
            task.setOnSucceeded((v) -> {
                tv_data.setDisable(false);
            });
            task.run();
        });
        //exit operation
        mi_exit.setOnAction(event -> {
            Platform.exit();
        });
        //add type operation (very simple at the moment)
        mi_addType.setOnAction(event -> {
            TextInputDialog tid = new TextInputDialog();
            tid.setTitle(Language.getString("addTypeView_title"));
            tid.setHeaderText(Language.getString("addTypeView_header"));
            tid.setContentText(Language.getString("addTypeView_content"));
            tid.initOwner(stage);

            Optional<String> result = tid.showAndWait();
            if (result.isPresent() && !result.get().isEmpty()) {
                this.db.addGroup(result.get());
            }
        });
        //show types operation (very simple at the moment)
        mi_showTypes.setOnAction(event -> {
            String types = "";
            for (String s : this.db.getGroups()) {
                types += s + "\n";
            }

            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setHeaderText(Language.getString("mainView_alertTypesTitle"));

            a.setContentText(types);

            a.initOwner(stage);
            a.showAndWait();
        });
        mi_gitlab.setOnAction(event -> {
            //getHostServices().showDocument("https://www.google.de");
            //TODO: Not really a good way using swing utilities here but the JavaFX way above does not work
            SwingUtilities.invokeLater(() -> {
                try {
                    Desktop.getDesktop().browse(new URI(Language.getString("gitlab_url")));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            });
        });

        mi_version.setDisable(true);

        m_file.getItems().addAll(mi_load, mi_save, mi_saveAs, mi_exportHtml, mi_exit);
        m_types.getItems().addAll(mi_addType, mi_showTypes);
        m_about.getItems().addAll(mi_version, mi_gitlab);
        mb_main.getMenus().addAll(m_file, m_types, m_about);


        tf_search.setPromptText(Language.getString("mainView_tfSearch"));
        tf_search.textProperty().addListener((observable, oldValue, newValue) -> {
            this.searchString = newValue;
            this.getDbData();
        });
        btn_clear.setOnAction((e) -> {
            tf_search.textProperty().set("");
        });

        tv_data.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        tv_data.getColumns().addAll(tc_link, tc_type, tc_gccode, tc_width, tc_height, tc_comment);
        tc_link.setCellValueFactory(new PropertyValueFactory<>("link"));
        tc_type.setCellValueFactory(data -> new ReadOnlyStringWrapper(db.getGroup(data.getValue())));
        tc_gccode.setCellValueFactory(new PropertyValueFactory<>("gccode"));
        tc_width.setCellValueFactory(new PropertyValueFactory<>("width"));
        tc_height.setCellValueFactory(new PropertyValueFactory<>("height"));
        tc_comment.setCellValueFactory(new PropertyValueFactory<>("comment"));
        tv_data.setItems(this.ol_data);

        btn_add.setOnAction(event -> {
            new BannerView(stage).showView(this.db.getGroups(), (b, g) -> {
                this.db.add(b, g);
                this.getDbData();
            });
        });
        btn_change.setOnAction(event -> {
            if (tv_data.getSelectionModel().getSelectedItem() != null) {
                Banner banner = tv_data.getSelectionModel().getSelectedItem();
                new BannerView(stage, banner, this.db.getGroup(banner)).showView(this.db.getGroups(), (b, g) -> {
                    this.db.triggerChanged();
                    //check if group has changed
                    if (this.db.getGroup(banner).compareTo(g) != 0) {
                        //group changed
                        this.db.remove(banner);
                        this.db.add(b, g);
                    }
                    //update tableview and database
                    this.getDbData();
                });
            }
        });
        btn_remove.setOnAction(event -> {
            ObservableList<Banner> temp = tv_data.getSelectionModel().getSelectedItems();
            for (Banner b : temp) {
                this.db.remove(b);
            }
            this.getDbData();
        });

        fp.setPadding(new Insets(5));
        fp.setHgap(5);
        fp.setVgap(5);
        fp.getChildren().addAll(btn_add, btn_change, btn_remove);

        hb_search.setSpacing(5);
        hb_search.setPadding(new Insets(5,5,0,5));
        HBox.setHgrow(tf_search, Priority.ALWAYS);
        hb_search.getChildren().addAll(tf_search, btn_clear);

        vb.setSpacing(5);
        VBox.setVgrow(tv_data, Priority.ALWAYS);
        vb.getChildren().addAll(hb_search, tv_data);

        bp.setTop(mb_main);
        bp.setCenter(vb);
        bp.setBottom(fp);

        Scene scene = new Scene(bp);

        tv_data.requestFocus();

        stage.setScene(scene);
        stage.show();

        Platform.setImplicitExit(false);

        //important ask what to do with unsaved changes at exit
        stage.setOnCloseRequest((event) -> {
            event.consume();

            if (this.db.hasUnsavedChanges()) {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                a.setTitle(Language.getString("mainView_alertSaveOnExitTitle"));
                a.setContentText(Language.getString("mainView_alertSaveOnExit"));

                ButtonType saveButton = new ButtonType(Language.getString("mainView_alertSaveOnExitSave"));
                ButtonType discardButton = new ButtonType(Language.getString("mainView_alertSaveOnExitDiscard"));
                ButtonType cancelButton = new ButtonType(Language.getString("mainView_alertSaveOnExitCancel"), ButtonBar.ButtonData.CANCEL_CLOSE);

                a.getButtonTypes().setAll(saveButton, discardButton, cancelButton);
                a.initOwner(stage);

                Optional<ButtonType> result = a.showAndWait();
                if (result.get() == saveButton) {
                    if (this.db.hasFormatter()) {
                        this.saveFile();
                    } else {
                        FileChooser fc = new FileChooser();
                        fc.setTitle(Language.getString("mainView_fcSave"));
                        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Language.getString("mainView_fcExtensionFilter"), "*.gcbm"));
                        fc.setInitialFileName(Language.getString("defaultFileName_saveFile") + ".gcbm");
                        File f = fc.showSaveDialog(this.stage);
                        if (f != null) {
                            this.saveFileAs(f);
                        }
                    }
                    Platform.exit();
                } else if(result.get() == discardButton) {
                    Platform.exit();
                }
            } else {
                Platform.exit();
            }
        });
    }

    private void loadFile(File f) {
        this.ol_data.clear();
        this.db.clearDB();
        this.db.setFormatter(new JSONStorage(new BannerJSONConverter(), new FileStorage(f)));
        this.db.loadDB();
        this.getDbData();
    }

    private void saveFile() {
        this.db.saveDB();
    }

    private void saveFileAs(File f) {
        this.db.setFormatter(new JSONStorage(new BannerJSONConverter(), new FileStorage(f)));
        this.db.saveDB();
    }

    private void exportHTML(File f) {
        IFormatter is = this.db.getFormatter();
        this.db.setFormatter(new HTMLStorage(new BannerHTMLConverter(), new FileStorage(f)));
        this.db.saveDB();
        this.db.setFormatter(is);
    }

    private void getDbData() {
        this.ol_data.clear();
        this.ol_data.addAll(this.db.getFiltered(this.searchString.split(",")));
    }

    /*private void stringToBanner() {
        String s = "";

        String[] sa = s.split("\n");

        for (int i = 0; i < sa.length; i++) {

            System.out.println(i);

            try {
                String link = Utility.tagTextToValue(sa[i], "img", "src");
                int type = 10;
                String[] linkarray = Utility.tagTextToValue(sa[i], "a", "href").split("/");
                String gccode = linkarray[linkarray.length-1];
                int height = Integer.parseInt(Utility.tagTextToValue(sa[i], "img", "height"));

                Banner b = new Banner(link);
                b.setGccode(gccode);
                b.setHeight(height);
                b.setType(type);

                this.ol_data.add(b);
                this.db.addBanner(b);

            } catch (TagTextToValueException e) {
                e.printStackTrace();
            }
        }
    }*/

    public static void main(String[] args) {
        launch(args);
    }
}
