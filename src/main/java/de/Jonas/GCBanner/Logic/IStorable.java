package de.Jonas.GCBanner.Logic;

public interface IStorable {
    boolean contains(String s);
}
