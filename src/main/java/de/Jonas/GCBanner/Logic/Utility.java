package de.Jonas.GCBanner.Logic;

import de.Jonas.Exceptions.TagTextToValueException;

public class Utility {

    public static String tagTextToValue(String rawText, String tag, String attribute) throws TagTextToValueException {
        int pos;

        //search for first tag
        pos = rawText.indexOf("<" + tag);
        if (pos == -1) {
            //tag not found
            throw new TagTextToValueException("tag start not found");
        }
        //cut off the leading text before the tag
        rawText = rawText.substring(pos);

        //search the end of the tag
        pos = rawText.indexOf(">");
        if (pos == -1) {
            //end not found
            throw new TagTextToValueException("tag end not found");
        }
        //cut off the text after tag end
        rawText = rawText.substring(0, pos + 1);



        //the rawText variable now contains just the tag


        //find the attribute
        pos = rawText.indexOf(attribute);
        if (pos == -1) {
            //attribute not found
            throw new TagTextToValueException("attribute not found");
        }
        //cut away everything up to the attribute's content
        rawText = rawText.substring(pos + attribute.length()).trim();

        pos = rawText.indexOf("=");
        if (pos != 0) {
            //= should have been the first character. There seems something wrong!
            throw new TagTextToValueException("attribute assignment not found");
        }
        //remove =
        rawText = rawText.substring(1).trim();

        //" of ' should now be the first character.

        if (rawText.charAt(0) == '"' || rawText.charAt(0) == '\'') {
            //remove "
            rawText = rawText.substring(1).trim();

            //now search for the next " (end of the attribute)
            String[] split = rawText.split("[\"']", 2);
            if (split.length == 1) {
                throw new TagTextToValueException("value end not found");
            }
            rawText = split[0];
        } else {
            //no " or ' provided. Not pretty but some people do this

            //search for the next space
            pos = rawText.indexOf(" ");
            if (pos == -1) {
                //next try: it could possibly be the last attribute -> end-tag
                System.out.println(rawText);
                pos = rawText.indexOf(">");
                if (pos == -1) {
                    //no space or end-tag found. This text is really wicked!
                    throw new TagTextToValueException("value end not found");
                }
            }
            rawText = rawText.substring(0, pos);
        }

        return rawText;
    }
}
