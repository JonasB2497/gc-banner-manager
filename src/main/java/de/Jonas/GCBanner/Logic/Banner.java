package de.Jonas.GCBanner.Logic;

import java.util.Date;

public class Banner implements IStorable {

    private String link;
    private String gccode = "";
    private int width;
    private int height;
    private String comment = "";
    private Date creationDate;

    public Banner(String link) {
        this.link = (link == null) ? "" : link;
    }

    public void setLink(String link) { this.link = (link == null) ? "" : link; }
    public void setGccode(String code) { this.gccode = (code == null) ? "" : code.toUpperCase(); }
    public void setWidth(int width) { this.width = width; }
    public void setHeight(int height) { this.height = height; }
    public void setComment(String comment) { this.comment = (comment == null) ? "" : comment; }
    public void setCreationDate(Date date) { this.creationDate = date; }

    public String getLink() { return this.link; }
    public String getGccode() { return this.gccode; }
    public int getWidth() { return this.width; }
    public int getHeight() { return this.height; }
    public String getComment() { return this.comment; }
    public Date getCreationDate() { return this.creationDate; }

    public boolean contains(String s) {
        s = s.toLowerCase().trim();
        return (this.link.toLowerCase().trim().contains(s) ||
                this.gccode.toLowerCase().trim().contains(s) ||
                this.comment.toLowerCase().trim().contains(s) ||
                String.valueOf(this.width).contains(s) ||
                String.valueOf(this.height).contains(s));
    }

    @Override
    public String toString() {
        return this.getGccode() + ", " + this.getWidth() + "x" + this.getHeight() + ", " + this.getLink() + ", " + this.getComment() + ", " + this.getCreationDate();
    }
}
