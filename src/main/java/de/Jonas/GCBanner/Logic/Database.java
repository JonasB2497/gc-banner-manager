package de.Jonas.GCBanner.Logic;

import de.Jonas.GCBanner.Storage.IFormatter;
import de.Jonas.GCBanner.Storage.IStorage;

import java.util.*;

public class Database<T extends IStorable> {

    private Map<String, List<T>> data = new HashMap<>();
    private IFormatter<T, String> formatter;
    private boolean changedAfterSave = false;

    public void add(T t, String group) {
        this.data.get(group).add(t);
        this.changedAfterSave = true;
    }
    public void addGroup(String name) {
        this.data.put(name, new ArrayList<T>());
        this.changedAfterSave = true;
    }

    /**
     * removes Item from every group available
     * @param t victim
     */
    public void remove(T t) {
        this.data.forEach((s, item) -> {
            item.remove(t);
        });
        this.changedAfterSave = true;
    }


    /**
     * @return count of all elements in the datanase
     */
    public int size() {
        int size = 0;
        for (String s : this.data.keySet()) {
            size += this.data.get(s).size();
        }
        return size;
    }

    /**
     * @param index item index
     * @param group group name
     * @return element at specified position
     */
    public T get(int index, String group) {
        return this.data.get(group).get(index);
    }

    /**
     * @param keys search criteria (no regexp)
     * @return a List that contains only the matching elements
     */
    public List<T> getFiltered(String... keys) {
        List<T> lb = new ArrayList<>();

        for (String key : this.data.keySet()) {
            List<T> item = this.data.get(key);
            for (T t : item) {
                boolean isInDB = true;
                for (String k : keys) {
                    if (!t.contains(k)) {
                        isInDB = false;
                        break;
                    }
                }
                if (isInDB) {
                    lb.add(t);
                }
            }
        }

        return lb;
    }

    /**
     * Identifies a group which contains the given element
     * @param t element
     * @return name of the group
     */
    public String getGroup(T t) {
        for (String key : this.data.keySet()) {
            if (this.data.get(key).contains(t)) {
                return key;
            }
        }
        return null;
    }

    /**
     * @return a list which contains all groupnames
     */
    public String[] getGroups() {
        return this.data.keySet().toArray(new String[this.data.size()]);
    }

    /**
     * a manual way to tell the database it got changed
     */
    public void triggerChanged() { this.changedAfterSave = true; }
    public boolean hasUnsavedChanges() {
        return this.changedAfterSave;
    }

    /**
     * sets the storage mode
     * @param formatter storage object
     */
    public void setFormatter(IFormatter<T, String> formatter) { this.formatter = formatter; }

    /**
     * @return the currently used storage object
     */
    public IFormatter<T, String> getFormatter() { return this.formatter; }

    /**
     * @return true if the database has a storage
     */
    public boolean hasFormatter() { return this.formatter != null; }

    /**
     * clears everything in the database
     */
    public void clearDB() {
        this.data.clear();
        this.changedAfterSave = true;
    }

    /**
     * saves everything in the database permanently
     */
    public void saveDB() {
        this.formatter.save(this.data);
        this.changedAfterSave = false;
    }

    /**
     * loads everything in the database memory
     */
    public void loadDB() {
        this.formatter.get(this.data);
        this.changedAfterSave = false;
    }

    /**
     * prints all data
     */
    public void printData() {
        this.data.forEach((key, item) -> {
            for (T t : item) {
                System.out.println(t.toString());
            }
        });
    }
}
