package de.Jonas.GCBanner.Storage;

public abstract class IStorage {

    protected String data;

    public abstract boolean store();
    public abstract boolean load();

    public boolean store(String data) {
        this.data = data;
        return this.store();
    }

    public String loadAndGet() {
        this.load();
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }
    public String getData() {
        return this.data;
    }
}
