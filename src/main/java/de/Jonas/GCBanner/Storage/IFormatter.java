package de.Jonas.GCBanner.Storage;

import de.Jonas.GCBanner.Storage.Converter.IConverter;

import java.util.List;
import java.util.Map;

/**
 * @param <T> the internal object representation
 * @param <K> the external object representation (like String in a file)
 */
public abstract class IFormatter<T, K> {

    protected IConverter<T, K> converter;
    protected IStorage storage;

    public IFormatter(IConverter<T, K> converter, IStorage storage) {
        this.converter = converter;
        this.storage = storage;
    }

    public abstract boolean save(Map<String, List<T>> data);
    public abstract void get(Map<String, List<T>> data);
}
