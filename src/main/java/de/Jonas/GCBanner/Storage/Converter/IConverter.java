package de.Jonas.GCBanner.Storage.Converter;

import java.util.Comparator;

/**
 * @param <T> the internal object representation
 * @param <K> the external object representation (like String in a file)
 */
public interface IConverter<T, K> extends Comparator<T> {
    K convertExport(T data);
    T convertImport(K data);
    K headerWrapper(String s);
    K groupSpacer();
}
