package de.Jonas.GCBanner.Storage.Converter;

import de.Jonas.GCBanner.Logic.Banner;

public class BannerHTMLConverter implements IConverter<Banner, String> {
    @Override
    public String convertExport(Banner b) {

        String s = "";

        if (!b.getGccode().isEmpty()) {
            s += "<a href=\"https://www.geocaching.com/geocache/" + b.getGccode() + "\">";
        }

        s += "<img src=\"" + b.getLink() + "\"";
        if (b.getWidth() > 0) {
            s += " width=\"" + b.getWidth() + "\"";
        }
        if (b.getHeight() > 0) {
            s += " height=\"" + b.getHeight() + "\"";
        }
        s += " />";

        if (!b.getGccode().isEmpty()) {
            s += "</a>";
        }

        return s;
    }

    @Override
    public Banner convertImport(String s) {
        return null;
    }

    @Override
    public String headerWrapper(String s) {
        return "<h2>" + s + "</h2><br>";
    }

    @Override
    public String groupSpacer() {
        return "<br><br><br>";
    }

    @Override
    public int compare(Banner b1, Banner b2) {
        return b1.getCreationDate().compareTo(b2.getCreationDate());
    }
}
