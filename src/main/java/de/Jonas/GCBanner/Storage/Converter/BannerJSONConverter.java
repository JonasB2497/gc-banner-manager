package de.Jonas.GCBanner.Storage.Converter;

import de.Jonas.GCBanner.Logic.Banner;
import org.json.JSONObject;

import java.util.Date;

public class BannerJSONConverter implements IConverter<Banner, JSONObject> {
    @Override
    public JSONObject convertExport(Banner b) {
        JSONObject o = new JSONObject();
        o.put("link", b.getLink());
        o.put("gccode", b.getGccode());
        o.put("width", b.getWidth());
        o.put("height", b.getHeight());
        o.put("comment", b.getComment());
        o.put("creationDate", b.getCreationDate().getTime());
        return o;
    }

    @Override
    public Banner convertImport(JSONObject data) {
        Banner b = new Banner(data.getString("link"));
        if (data.has("gccode")) {
            b.setGccode(data.getString("gccode"));
        }
        if (data.has("width")) {
            b.setWidth(data.getInt("width"));
        }
        if (data.has("height")) {
            b.setHeight(data.getInt("height"));
        }
        if (data.has("comment")) {
            b.setComment(data.getString("comment"));
        }
        if (data.has("creationDate")) {
            b.setCreationDate(new Date(data.getLong("creationDate")));
        }
        return b;
    }

    @Override
    public JSONObject headerWrapper(String s) {
        return null;
    }

    @Override
    public JSONObject groupSpacer() {
        return null;
    }

    @Override
    public int compare(Banner banner, Banner t1) {
        return 0;
    }
}
