package de.Jonas.GCBanner.Storage;

import de.Jonas.GCBanner.GUI.Language;
import de.Jonas.GCBanner.Storage.Converter.IConverter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSONStorage<T> extends IFormatter<T, JSONObject> {

    public JSONStorage(IConverter<T, JSONObject> converter, IStorage storage) {
        super(converter, storage);
    }

    @Override
    public boolean save(Map<String, List<T>> data) {

        JSONObject jso = new JSONObject();
        JSONArray jsaData = new JSONArray();
        JSONArray jsaGroups = new JSONArray();

        //put groups and data in jsonArray
        for (String s : data.keySet()) {
            jsaGroups.put(s);

            List<T> lt = data.get(s);
            JSONArray temp = new JSONArray();
            for (T t : lt) {
                temp.put(this.converter.convertExport(t));
            }
            jsaData.put(temp);
        }

        jso.put("groups", jsaGroups);
        jso.put("data", jsaData);
        jso.put("version", Language.getString("version"));

        return this.storage.store(jso.toString(2));
    }

    @Override
    public void get(Map<String, List<T>> data) {
        JSONObject jso = new JSONObject(this.storage.loadAndGet());
        JSONArray jsa_groups = jso.getJSONArray("groups");
        JSONArray jsa_data = jso.getJSONArray("data");

        //get the groups
        for (int i = 0; i < jsa_groups.length(); i++) {
            JSONArray temp = jsa_data.getJSONArray(i);
            List<T> t = new ArrayList<>();
            for(int j = 0; j < temp.length(); j++) {
                t.add(this.converter.convertImport(temp.getJSONObject(j)));
            }

            data.put(jsa_groups.getString(i), t);
        }
    }
}
