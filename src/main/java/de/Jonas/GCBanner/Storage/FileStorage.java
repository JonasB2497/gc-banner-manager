package de.Jonas.GCBanner.Storage;

import java.io.*;

public class FileStorage extends IStorage {

    private final File f;

    public FileStorage(File f) {
        this.f = f;
    }

    @Override
    public boolean store() {
        try (FileWriter fw = new FileWriter(this.f); BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(this.data);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean load() {
        try (Reader r = new FileReader(this.f); BufferedReader br = new BufferedReader(r)) {
            this.data = "";
            while (br.ready()) {
                this.data += br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
