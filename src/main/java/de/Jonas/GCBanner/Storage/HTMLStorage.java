package de.Jonas.GCBanner.Storage;

import de.Jonas.GCBanner.Storage.Converter.IConverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HTMLStorage<T> extends IFormatter<T, String> {

    private boolean printStatistic = false;

    public HTMLStorage(IConverter<T, String> converter, IStorage storage) {
        super(converter, storage);
    }

    @Override
    public boolean save(Map<String, List<T>> data) {

        String stringData = "";

        List<T>[] lt = new ArrayList[data.size()];
        List<String> groups = new ArrayList<>(data.keySet());

        //initialize lists
        for (int i = 0; i < lt.length; i++) {
            lt[i] = new ArrayList<>(data.get(groups.get(i)));
        }

        //sort buckets by creation date
        for(List<T> l : lt) {
            l.sort(this.converter);
        }



        //print statistics
        if (this.printStatistic) {
            stringData += "<table>";
            for (int i = 0; i < lt.length; i++) {
                if (lt[i].size() != 0) {
                    stringData += "<tr><td>" + groups.get(i) + ":</td><td>" + lt[i].size() + "</td></tr>";
                }
            }
            stringData += "</table>" + this.converter.groupSpacer();
        }

        //print all images out of each bucket
        for (int i = 0; i < lt.length; i++) {
            if (lt[i].size() != 0) {
                //add the type as header to the output string
                stringData += this.converter.headerWrapper(groups.get(i)) + "\n";

                for (T t : lt[i]) {
                    stringData += this.converter.convertExport(t);
                    stringData += "\n";
                }
                stringData += this.converter.groupSpacer() + "\n";
            }
        }

        return this.storage.store(stringData);
    }

    @Override
    public void get(Map<String, List<T>> data) {

    }
}
